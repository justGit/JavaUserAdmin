package org.gpf.factory;

import org.gpf.dao.IUserDAO;
import org.gpf.dao.impl.proxy.UserDAOProxy;

/**
 * 静态工厂，返回DAO的代理
 * @author gaopengfei
 * @date 2015-4-23 下午12:30:27
 */
public class DAOFactory {

	public static IUserDAO getIUserDAOInstance(){
		return new UserDAOProxy();
	}
	
}
