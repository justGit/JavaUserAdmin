package org.gpf.test;

import java.util.Date;

import org.gpf.factory.DAOFactory;
import org.gpf.vo.User;

public class TestInsert {

	public static void main(String[] args) {

		User user = new User();
		user.setUsername("Tom");
		user.setPassword("123456");
		user.setAge(22);
		user.setSex("男");
		user.setBirthday(new Date());
		DAOFactory.getIUserDAOInstance().doCreate(user);
	}

}
