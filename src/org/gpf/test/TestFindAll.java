package org.gpf.test;

import java.util.Iterator;
import java.util.List;

import org.gpf.factory.DAOFactory;
import org.gpf.vo.User;

public class TestFindAll {

	public static void main(String[] args) {

		List<User> users = DAOFactory.getIUserDAOInstance().findAll("");
		Iterator<User>iterator = users.iterator();
		while (iterator.hasNext()) {
			User user = (User) iterator.next();
			System.out.println(user);
		}
	}

}
