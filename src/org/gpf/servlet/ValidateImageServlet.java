package org.gpf.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 * 对验证码进行校验的Servlet
 * @author gaopengfei
 * @date 2015-5-8 下午8:36:06
 */
@SuppressWarnings("serial")
public class ValidateImageServlet extends HttpServlet {

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();	
		
		String checkCode = request.getParameter("checkcode");
		String picCode = (String) request.getSession().getAttribute("piccode");
		
		if (checkCode != null && picCode != null && checkCode.equalsIgnoreCase(picCode)) 
			out.print("validate");
		else {
			out.print("invalidate");
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		doGet(request, response);
	}

}
