package org.gpf.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class IPTimeStamp {

	private SimpleDateFormat sdf;
	private String ip;

	public IPTimeStamp() {

	}

	public IPTimeStamp(String ip) {
		this.ip = ip;
	}

	/**
	 * IP+时间戳+随机数
	 */
	public String getIPTimeRand() {

		StringBuffer sb = new StringBuffer();

		if (this.ip != null) {
			String[] s = this.ip.split("\\."); // 注意：.需要转义操作
			for (String string : s) {
				sb.append(this.addZero(string, 3));
			}
		}

		sb.append(this.getTimeStamp());
		Random r = new Random();
		for (int i = 0; i < 3; i++) {
			sb.append(r.nextInt(10));
		}
		return sb.toString();
	}

	/**
	 * 返回时间戳
	 * @return
	 */
	public String getTimeStamp() {

		this.sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
		return this.sdf.format(new Date());
	}

	/**
	 * 返回指定位数的数字，位数不够前面补0
	 */
	private String addZero(String str, int len) {

		StringBuffer sb = new StringBuffer();
		sb.append(str);
		while (sb.length() < len) {
			sb.insert(0, 0); // 在第0个位置补0
		}
		return sb.toString();
	}

}
