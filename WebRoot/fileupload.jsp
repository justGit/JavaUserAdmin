<%@page import="java.io.File"%>
<%@page import="org.gpf.util.FileUploadTool"%>
<%@ page language="java" import="java.util.*" contentType="text/html; charset=utf-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>fileupload.jsp</title>
  </head>
  
  <body>
<% 
	FileUploadTool fut = new FileUploadTool(request,3*1024*1024,getServletContext().getRealPath("/") + "uploadtemp");
	String name = fut.getParameter("uname");
	String[] inst = fut.getParameterValues("inst");
	List<String> all = fut.saveAll(getServletContext().getRealPath("/") + "file_upload" + File.separator);
%>
	
	姓名：<%=name %><br />
	兴趣：
<%--
	使用EL表达式进行输出
<%
	if (inst != null){
		for (int i = 0;i < inst.length;i++) {
			pageContext.setAttribute("inst", inst[i]);
%>
			${inst }
<%	
		}
	}
%>	
--%>
	<!-- 使用JSTL进行输出 -->
	<% pageContext.setAttribute("inst", inst); %>
	 <%@taglib prefix="c" uri="http://127.0.0.1/JavaUserAdmin/jstl/core" %>
	 <c:forEach items="${inst }" var="ins">${ins } </c:forEach>
	 
	 
<%--
	使用EL表达式输出
	Iterator<String> iterator = all.iterator();
	while(iterator.hasNext()){
		pageContext.setAttribute("filename", iterator.next());
--%>
	<!-- <img alt="" src="file_upload/${filename }"> -->
<%--	
	}
 --%>
 
 <!-- 使用JSTL进行输出 -->
 <%pageContext.setAttribute("all", all); %>
 <c:forEach items="${all }" var="pic">
 	<img alt="" src="file_upload/${pic }">
 </c:forEach>
  </body>
</html>
