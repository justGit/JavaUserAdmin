# 人员管理系统 #
主要运用到的知识：OO、JDBC、JSP、集合框架。

该项目使用到了两种视图显示给用户：控制台视图和web视图。

- 控制台视图：menu、operation
- web视图：各种jsp页面和html页面

web视图采用model2模式开发【jsp（V）+javabean（M）+servlet（C）】，采用MVC分层架构。

## 需求分析 ##
开发一个用户管理程序，其中用户的基本信息包括：

- 用户编号
- 用户姓名
- 性别
- 生日

本程序可以有两种视图

1. 控制台视图
2. jsp视图


要求用程序实现用户的统一管理（增删改查），所有的信息保存在数据库之中。

## 实现的步骤 ##
### 一、创建数据库脚本 ###

    DROP TABLE IF EXISTS `user`;
	CREATE TABLE `user` (
	  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
	  `name` varchar(30) NOT NULL,
	  `sex` varchar(30) ,
	  `age` int(11),
	  `birthday` date
	) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


### 二、接口的设计 ###
IUserDAO（DAO表示数据操作对象，可以操作数据），接口准备完成之后将表中的字段进行一些抽象，做一个简单的java实体类`User.java`。

    public class User {

		private int id;
		private String name;
		private String sex;
		private int age;
		private Date birthday;
		
		// getter和setter	
	}

下面完善以上的IUserDAO接口。
    
	/**
	 * 数据访问接口
	 */
	public interface IUserDAO {
	
		public boolean doCreate(User user); 		// DB创建
		public boolean doUpdate(User user);			// DB更新
		public boolean doDelete(int id);			// DB删除
		public User findById(int id);				// 根据id查找
		public List<User> findAll(String keyword);	// 查询多个
	}

在接口完善以后，实际上对于表的一个完整的操作标准就制定出来了。下面需要一个专门负责DB连接的操作类。

### 三、数据库的连接类 ###
该类专门负责数据库的连接和关闭操作

### 四、实现IUserDAO接口 ###

# 使用中文关键字搜索，出现 Illegal mix of collations for operation &#39;like&#39; 错误解决方法 #
**错误原理分析以及错误解决方法：**

在 MySQL 5.5 以上, 若字段 Type 是 time,date,datetime 
在查询时若使用 `like '%中文%' `会出现 `Illegal mix of collations for operation 'like'`

在写程序时要对每个字段进行搜索，在执行时可能就会出现时间字段 `like '%中文%' `这种语法，
这在比较久的版本MySQL是不会出现错误的。

但是升级到MySQL 5.5以上，必需改成`like binary '%中文%'` 即可避免出现错误。